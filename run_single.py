# SMART360 Simulator
# Copyright (C) 2023  Quentin Guimard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import argparse

from simulator import Session


parser = argparse.ArgumentParser(
    prog='SMART360',
    description='SMART360: Simulating Motion prediction and Adaptive bitRate sTrategies for 360° video streaming',
)

parser.add_argument('-m', '--manifest',
                    dest='manifest',
                    default='./config/real/video_manifests/Waterfall/12x6_1000.json',
                    help='Path to source JSON video manifest.')

parser.add_argument('-c', '--coord_trace',
                    dest='coord_trace',
                    default='./config/real/head_motion_traces/Waterfall/liantianye_m1.json',
                    help='Path to source JSON head motion coordinates trace.')

parser.add_argument('-n', '--network_trace',
                    dest='network_trace',
                    default='./config/real/network_traces/scaled/bus_0006.json',
                    help='Path to source JSON network trace.')

parser.add_argument('-l', '--log_file',
                    dest='log_file',
                    default='./test_output/session_logs.json',
                    help='Path to destination JSON log file.')

parser.add_argument('-a', '--ABR',
                    dest='ABR',
                    default='BaselineABR',
                    help='Name of the adaptive bitrate algorithm used to allocate quality during the simulation.')

parser.add_argument('-p', '--pred',
                    dest='pred',
                    default='StaticPredictor',
                    help='Name of the viewport prediction algorithm used by the ABR during the simulation.')

parser.add_argument('-K',
                    dest='K',
                    type=int,
                    default=1,
                    help='Number of predictions to make (if viewport prediction algorithm allows it).')

parser.add_argument('-r', '--likelihood_window',
                    dest='r',
                    type=int,
                    default=15,
                    help='Window size used for likelihood estimation based on past error (if viewport prediction algorithm allows it).')

parser.add_argument('--B_min',
                    dest='B_min',
                    type=int,
                    default=5,
                    help='ABR constraint for minimum target buffer length (in number of segments).')

parser.add_argument('--B_max',
                    dest='B_max',
                    type=int,
                    default=10,
                    help='Maximum size of the buffer (in number of segments).')

parser.add_argument('-d', '--delta_dl',
                    dest='delta_dl',
                    type=int,
                    default=1000,
                    help='Interval between download requests (in milliseconds).')

parser.add_argument('--EWMA_alpha',
                    dest='EWMA_alpha',
                    default='1.2',
                    help='Decay coefficient used in EWMA bandwidth and latency estimation.')

session = Session(vars(parser.parse_args()))
session.run()
session.end()
