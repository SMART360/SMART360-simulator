# SMART360 Simulator
# Copyright (C) 2023  Quentin Guimard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import multiprocessing
import os

import numpy as np
import pandas as pd
from tqdm.auto import tqdm

BASE_DIR = '/SMART360-simulator/'
SRC_DIR = BASE_DIR + 'output'
SRC_LIM = len(SRC_DIR) + 1
CONCATENATED_DIR = BASE_DIR + 'notebooks/concatenated_df'
METRICS_DIR = BASE_DIR + 'notebooks/metrics_df'
CONCATENATED_METRICS_DIR = BASE_DIR + 'notebooks/concatenated_metrics_df'


def concatenate_dataframes(item):
    (v, bmin, df_name), files = item
    dataframes = []
    for file in files:
        ABR, pred, K, B_min, B_max, video, user, tiling, segment, network, delta_dl, r, EWMA_alpha, _ = file[
                                                                                                        SRC_LIM:].split(
            '/')
        df = pd.read_feather(file)
        df['ABR'] = ABR.split('-')[0]
        df['pred'] = pred
        df['K'] = int(K.split('=')[1])
        df['B_min'] = int(B_min.split('=')[1])
        df['B_max'] = int(B_max.split('=')[1])
        df['video'] = video.split('-')[1]
        df['user'] = user.split('-')[1]
        df['tiling'] = tiling.split('-')[0]
        df['segment_size'] = int(segment.split('-')[0])
        df['network_trace'] = network.split('-')[1]
        df['delta_dl'] = int(delta_dl.split('=')[1])
        df['r'] = int(r.split('=')[1])
        df['EWMA_alpha'] = float(EWMA_alpha.split('=')[1])
        dataframes.append(df)
    df = pd.concat(dataframes).reset_index(drop=True)
    os.makedirs(f'{CONCATENATED_DIR}/{v}/{bmin}', exist_ok=True)
    df.to_feather(f'{CONCATENATED_DIR}/{v}/{bmin}/{df_name}.feather')


def concatenate_all_dataframes(n_cpus=max(1, os.cpu_count() - 2)):
    files_dict = {}
    for ABR in sorted(os.listdir(SRC_DIR)):
        for pred in sorted(os.listdir(f'{SRC_DIR}/{ABR}')):
            for K in sorted(os.listdir(f'{SRC_DIR}/{ABR}/{pred}')):
                for B_min in sorted(os.listdir(f'{SRC_DIR}/{ABR}/{pred}/{K}/')):
                    bmin = B_min.split('=')[1]
                    for B_max in sorted(os.listdir(f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}')):
                        for video in sorted(os.listdir(f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}')):
                            v = video.split('-')[1]
                            for user in sorted(os.listdir(f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}/{video}')):
                                for tiling in sorted(
                                        os.listdir(f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}/{video}/{user}')):
                                    for segment in sorted(os.listdir(
                                            f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}/{video}/{user}/{tiling}')):
                                        for network in sorted(os.listdir(
                                                f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}/{video}/{user}/{tiling}/{segment}')):
                                            for delta_dl in sorted(os.listdir(
                                                    f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}/{video}/{user}/{tiling}/{segment}/{network}')):
                                                for r in sorted(os.listdir(
                                                        f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}/{video}/{user}/{tiling}/{segment}/{network}/{delta_dl}')):
                                                    for EWMA_alpha in sorted(os.listdir(
                                                            f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}/{video}/{user}/{tiling}/{segment}/{network}/{delta_dl}/{r}')):
                                                        for df_name in ['bw_util', 'downloaded', 'stall', 'startup']:
                                                            file_name = f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}/{video}/{user}/{tiling}/{segment}/{network}/{delta_dl}/{r}/{EWMA_alpha}/logs-{df_name}.feather'
                                                            if os.path.exists(file_name):
                                                                try:
                                                                    files_dict[(v, bmin, df_name)].append(file_name)
                                                                except KeyError:
                                                                    files_dict[(v, bmin, df_name)] = [file_name]
                                                            else:
                                                                print(f'{file_name} does not exist!')
    with multiprocessing.Pool(n_cpus) as pool:
        with tqdm(total=len(files_dict)) as pbar:
            for _ in pool.imap_unordered(concatenate_dataframes, files_dict.items()):
                pbar.update()


def process_metrics(video_B_min):
    video, B_min = video_B_min
    os.makedirs(f'{METRICS_DIR}/{video}/{B_min}', exist_ok=True)
    downloaded_df = pd.read_feather(f'/{CONCATENATED_DIR}/{video}/{B_min}/downloaded.feather')

    downloaded_df['q_lin'] = 2 ** downloaded_df['quality']
    downloaded_df['q_log'] = downloaded_df['quality'] + 1
    downloaded_df = downloaded_df.drop(columns=['quality'])
    H_lin = downloaded_df['q_lin'].max()
    H_log = downloaded_df['q_log'].max()
    L_lin = downloaded_df['q_lin'].min()
    L_log = downloaded_df['q_log'].min()

    video_lengths = downloaded_df.drop(columns=['tile', 'dl_time', 'dl_head', 'first_request_time',
                                                'first_request_head', 'last_request_time', 'last_request_head',
                                                'n_requests', 'score', 'duration', 'size', 'cycle', 'ABR', 'pred', 'K',
                                                'B_min', 'B_max', 'user', 'tiling', 'segment_size', 'delta_dl', 'r',
                                                'EWMA_alpha',
                                                'network_trace', 'q_lin', 'q_log']).groupby(['video'],
                                                                                            as_index=False).max()
    video_lengths['length'] = video_lengths['segment'] + 1
    video_lengths = video_lengths.drop(columns=['segment'])

    baseline_visible_dl = downloaded_df[
        (downloaded_df['ABR'] == 'baseline') & (downloaded_df['duration'] > 0) & (downloaded_df['B_max'] == 10)].copy()
    baseline_visible_dl['dl_offset'] = baseline_visible_dl['last_request_head'] - (
            baseline_visible_dl['segment'] * baseline_visible_dl['segment_size'])
    baseline_visible_dl['discrete_dl_offset'] = np.round(baseline_visible_dl['dl_offset'] / 1000, decimals=0)
    baseline_visible_dl['discrete_dl_head'] = np.round(baseline_visible_dl['dl_head'] / 1000, decimals=0)

    baseline_visible_dl['weighted_q_lin'] = baseline_visible_dl['q_lin'] * baseline_visible_dl['duration']
    baseline_visible_dl['weighted_q_log'] = baseline_visible_dl['q_log'] * baseline_visible_dl['duration']
    baseline_visible_agg = baseline_visible_dl.drop(columns=['dl_time', 'dl_head', 'first_request_time',
                                                             'first_request_head', 'last_request_time',
                                                             'last_request_head', 'n_requests', 'score', 'size',
                                                             'cycle',
                                                             'dl_offset', 'discrete_dl_offset', 'discrete_dl_head',
                                                             'tile', 'q_lin', 'q_log']).groupby(
        ['ABR', 'pred', 'K', 'B_min', 'B_max',
         'video', 'user', 'tiling',
         'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace', 'segment'],
        as_index=False).sum()
    baseline_visible_agg['weighted_q_lin'] = baseline_visible_agg['weighted_q_lin'] / baseline_visible_agg['duration']
    baseline_visible_agg['weighted_q_log'] = baseline_visible_agg['weighted_q_log'] / baseline_visible_agg['duration']
    baseline_visible_agg = baseline_visible_agg.drop(columns=['duration'])
    baseline_visible_dl = baseline_visible_dl.drop(columns=['weighted_q_lin'])
    baseline_visible_dl = baseline_visible_dl.drop(columns=['weighted_q_log'])

    baseline_visible_dl.reset_index(drop=True).to_feather(f'{METRICS_DIR}/{video}/{B_min}/baseline_visible_dl.feather')
    baseline_visible_agg.reset_index(drop=True).to_feather(
        f'{METRICS_DIR}/{video}/{B_min}/baseline_visible_agg.feather')

    baseline_visible_agg_fairness = baseline_visible_agg.copy()
    baseline_visible_agg_fairness = baseline_visible_agg_fairness.groupby(['ABR', 'pred', 'K', 'B_min', 'B_max',
                                                                           'video', 'user', 'tiling',
                                                                           'segment_size', 'delta_dl', 'r',
                                                                           'EWMA_alpha', 'network_trace'],
                                                                          as_index=False).mean()
    baseline_visible_agg_fairness = baseline_visible_agg_fairness.groupby(['ABR', 'pred', 'K', 'B_min', 'B_max',
                                                                           'video', 'tiling', 'segment_size',
                                                                           'delta_dl', 'r', 'EWMA_alpha',
                                                                           'network_trace'],
                                                                          as_index=False).std()
    baseline_visible_agg_fairness['fairness_index_q_lin'] = 1 - (
            2 * baseline_visible_agg_fairness['weighted_q_lin'] / (H_lin - L_lin))
    baseline_visible_agg_fairness['fairness_index_q_log'] = 1 - (
            2 * baseline_visible_agg_fairness['weighted_q_log'] / (H_log - L_log))
    baseline_visible_agg_fairness = baseline_visible_agg_fairness.drop(
        columns=['segment', 'weighted_q_lin', 'weighted_q_log'])

    baseline_visible_agg_fairness.reset_index(drop=True).to_feather(
        f'{METRICS_DIR}/{video}/{B_min}/baseline_visible_agg_fairness.feather')

    baseline_visible_svar = pd.merge(baseline_visible_dl, baseline_visible_agg, 'left')
    baseline_visible_svar['spatial_q_lin_variance'] = ((baseline_visible_svar['q_lin'] - baseline_visible_svar[
        'weighted_q_lin']) ** 2) * baseline_visible_svar['duration']
    baseline_visible_svar['spatial_q_log_variance'] = ((baseline_visible_svar['q_log'] - baseline_visible_svar[
        'weighted_q_log']) ** 2) * baseline_visible_svar['duration']
    baseline_visible_svar = baseline_visible_svar.drop(columns=['dl_time', 'dl_head', 'first_request_time',
                                                                'first_request_head', 'last_request_time',
                                                                'last_request_head', 'n_requests', 'score', 'size',
                                                                'cycle',
                                                                'dl_offset', 'discrete_dl_offset', 'discrete_dl_head',
                                                                'tile',
                                                                'q_lin', 'q_log', 'weighted_q_lin',
                                                                'weighted_q_log']).groupby(
        ['ABR', 'pred', 'K', 'B_min', 'B_max',
         'video', 'user', 'tiling',
         'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace', 'segment'],
        as_index=False).sum()
    baseline_visible_svar['spatial_q_lin_variance'] = baseline_visible_svar['spatial_q_lin_variance'] / \
                                                      baseline_visible_svar['duration']
    baseline_visible_svar['spatial_q_log_variance'] = baseline_visible_svar['spatial_q_log_variance'] / \
                                                      baseline_visible_svar['duration']
    baseline_visible_svar = baseline_visible_svar.drop(columns=['duration'])

    baseline_visible_svar.reset_index(drop=True).to_feather(
        f'{METRICS_DIR}/{video}/{B_min}/baseline_visible_svar.feather')

    baseline_visible_svar_fairness = baseline_visible_svar.copy()
    baseline_visible_svar_fairness = baseline_visible_svar_fairness.groupby(['ABR', 'pred', 'K', 'B_min', 'B_max',
                                                                             'video', 'user', 'tiling',
                                                                             'segment_size', 'delta_dl', 'r',
                                                                             'EWMA_alpha', 'network_trace'],
                                                                            as_index=False).mean()
    baseline_visible_svar_fairness = baseline_visible_svar_fairness.groupby(['ABR', 'pred', 'K', 'B_min', 'B_max',
                                                                             'video', 'tiling', 'segment_size',
                                                                             'delta_dl', 'r', 'EWMA_alpha',
                                                                             'network_trace'],
                                                                            as_index=False).std()
    H_svar_lin = (((H_lin + L_lin) / 2) - L_lin) ** 2
    H_svar_log = (((H_log + L_log) / 2) - L_log) ** 2
    L_svar_lin = 0
    L_svar_log = 0
    baseline_visible_svar_fairness['fairness_index_q_lin'] = 1 - (
            2 * baseline_visible_svar_fairness['spatial_q_lin_variance'] / (H_svar_lin - L_svar_lin))
    baseline_visible_svar_fairness['fairness_index_q_log'] = 1 - (
            2 * baseline_visible_svar_fairness['spatial_q_log_variance'] / (H_svar_log - L_svar_log))
    baseline_visible_svar_fairness = baseline_visible_svar_fairness.drop(
        columns=['segment', 'spatial_q_lin_variance', 'spatial_q_log_variance'])

    baseline_visible_svar_fairness.reset_index(drop=True).to_feather(
        f'{METRICS_DIR}/{video}/{B_min}/baseline_visible_svar_fairness.feather')

    baseline_visible_tvar = baseline_visible_agg.copy()
    baseline_visible_tvar['temporal_q_lin_variance'] = abs(baseline_visible_tvar['weighted_q_lin'].diff())
    baseline_visible_tvar['temporal_q_log_variance'] = abs(baseline_visible_tvar['weighted_q_log'].diff())
    baseline_visible_tvar['temporal_q_lin_variance'] = np.where(baseline_visible_tvar['segment'] == 0, np.nan,
                                                                baseline_visible_tvar['temporal_q_lin_variance'])
    baseline_visible_tvar['temporal_q_log_variance'] = np.where(baseline_visible_tvar['segment'] == 0, np.nan,
                                                                baseline_visible_tvar['temporal_q_log_variance'])
    baseline_visible_tvar = baseline_visible_tvar.drop(columns=['segment',
                                                                'weighted_q_lin',
                                                                'weighted_q_log']).dropna().groupby(
        ['ABR', 'pred', 'K', 'B_min', 'B_max',
         'video', 'user', 'tiling',
         'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace'],
        as_index=False).mean()

    baseline_visible_tvar.reset_index(drop=True).to_feather(
        f'{METRICS_DIR}/{video}/{B_min}/baseline_visible_tvar.feather')

    baseline_visible_tvar_fairness = baseline_visible_tvar.copy()
    baseline_visible_tvar_fairness = baseline_visible_tvar_fairness.groupby(['ABR', 'pred', 'K', 'B_min', 'B_max',
                                                                             'video', 'tiling', 'segment_size',
                                                                             'delta_dl', 'r', 'EWMA_alpha',
                                                                             'network_trace'],
                                                                            as_index=False).std()
    H_tvar_lin = H_lin - L_lin
    H_tvar_log = H_log - L_log
    L_tvar_lin = 0
    L_tvar_log = 0
    baseline_visible_tvar_fairness['fairness_index_q_lin'] = 1 - (
            2 * baseline_visible_tvar_fairness['temporal_q_lin_variance'] / (H_tvar_lin - L_tvar_lin))
    baseline_visible_tvar_fairness['fairness_index_q_log'] = 1 - (
            2 * baseline_visible_tvar_fairness['temporal_q_log_variance'] / (H_tvar_log - L_tvar_log))
    baseline_visible_tvar_fairness = baseline_visible_tvar_fairness.drop(
        columns=['temporal_q_lin_variance', 'temporal_q_log_variance'])

    baseline_visible_tvar_fairness.reset_index(drop=True).to_feather(
        f'{METRICS_DIR}/{video}/{B_min}/baseline_visible_tvar_fairness.feather')

    del baseline_visible_dl
    del baseline_visible_agg_fairness
    del baseline_visible_svar_fairness
    del baseline_visible_tvar_fairness

    stall_df = pd.read_feather(f'{CONCATENATED_DIR}/{video}/{B_min}/stall.feather')
    exp_stall_df = pd.read_feather(
        f'{CONCATENATED_DIR}/{video}/{1}/stall.feather')  # TODO: specify chosen B_min somewhere

    min_stall = exp_stall_df[
        (exp_stall_df['ABR'] == 'trivial') & (exp_stall_df['B_max'] == exp_stall_df['B_max'].max())].copy()
    min_stall = min_stall.drop(columns=['ABR',
                                        'pred',
                                        'K',
                                        'B_min',
                                        'B_max',
                                        'segment',
                                        'play_head',
                                        'downloaded_bits']).groupby(['video',
                                                                     'user',
                                                                     'tiling',
                                                                     'segment_size', 'delta_dl', 'r', 'EWMA_alpha',
                                                                     'network_trace'],
                                                                    as_index=False).sum().drop(
        columns=['user']).groupby(['video',
                                   'tiling',
                                   'segment_size', 'delta_dl', 'r', 'EWMA_alpha',
                                   'network_trace'],
                                  as_index=False).mean().rename(columns={
        'duration': 'min_stall'
    })

    max_stall = exp_stall_df[
        (exp_stall_df['ABR'] == 'max_stall') & (exp_stall_df['B_max'] == exp_stall_df['B_max'].min())].copy()
    max_stall = max_stall.drop(columns=['ABR',
                                        'pred',
                                        'K',
                                        'B_min',
                                        'B_max',
                                        'segment',
                                        'play_head',
                                        'downloaded_bits']).groupby(['video',
                                                                     'user',
                                                                     'tiling',
                                                                     'segment_size', 'delta_dl', 'r', 'EWMA_alpha',
                                                                     'network_trace'],
                                                                    as_index=False).sum().drop(
        columns=['user']).groupby(['video',
                                   'tiling',
                                   'segment_size', 'delta_dl', 'r', 'EWMA_alpha',
                                   'network_trace'],
                                  as_index=False).mean().rename(columns={
        'duration': 'max_stall'
    })

    baseline_stall = stall_df[(stall_df['ABR'] == 'baseline')].copy()
    tmp_df = downloaded_df[downloaded_df['ABR'] == 'baseline'][
        ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'user', 'tiling', 'segment_size', 'network_trace', 'delta_dl',
         'r', 'EWMA_alpha', 'segment']].drop_duplicates()
    baseline_stall = pd.merge(tmp_df, baseline_stall, 'left').fillna(0)
    bad_play_head = baseline_stall[baseline_stall['play_head'] == 0]
    baseline_stall.loc[baseline_stall['play_head'] == 0, 'play_head'] = bad_play_head['segment'] * bad_play_head[
        'segment_size'] + bad_play_head['segment_size'] // 2

    del tmp_df
    del bad_play_head

    baseline_stall.reset_index(drop=True).to_feather(f'{METRICS_DIR}/{video}/{B_min}/baseline_stall.feather')

    baseline_stall_agg = baseline_stall.drop(columns=['segment',
                                                      'play_head',
                                                      'downloaded_bits']).groupby(['ABR', 'pred', 'K', 'B_min',
                                                                                   'B_max', 'video', 'user', 'tiling',
                                                                                   'segment_size', 'delta_dl', 'r',
                                                                                   'EWMA_alpha', 'network_trace'],
                                                                                  as_index=False).sum()

    baseline_stall_agg.reset_index(drop=True).to_feather(f'{METRICS_DIR}/{video}/{B_min}/baseline_stall_agg.feather')

    baseline_stall_agg_fairness = baseline_stall_agg.groupby(['ABR', 'pred', 'K', 'B_min',
                                                              'B_max', 'video', 'tiling',
                                                              'segment_size', 'delta_dl', 'r', 'EWMA_alpha',
                                                              'network_trace'],
                                                             as_index=False).std(numeric_only=True)
    baseline_stall_agg_fairness = pd.merge(baseline_stall_agg_fairness, min_stall, 'left').fillna(0.0)
    baseline_stall_agg_fairness = pd.merge(baseline_stall_agg_fairness, max_stall, 'left')
    baseline_stall_agg_fairness['fairness_index'] = 1 - (2 * baseline_stall_agg_fairness['duration'] / (
            baseline_stall_agg_fairness['max_stall'] - baseline_stall_agg_fairness['min_stall']))
    baseline_stall_agg_fairness = baseline_stall_agg_fairness.drop(columns=['duration', 'min_stall', 'max_stall'])

    baseline_stall_agg_fairness.reset_index(drop=True).to_feather(
        f'{METRICS_DIR}/{video}/{B_min}/baseline_stall_agg_fairness.feather')

    del min_stall
    del max_stall
    del stall_df
    del baseline_stall
    del baseline_stall_agg_fairness

    baseline_qoe = pd.merge(
        pd.merge(baseline_visible_agg.drop(columns=['segment']).groupby(['ABR', 'pred', 'K', 'B_min', 'B_max', 'video',
                                                                         'user', 'tiling', 'segment_size', 'delta_dl',
                                                                         'r', 'EWMA_alpha',
                                                                         'network_trace'],
                                                                        as_index=False).mean(),
                 baseline_stall_agg.rename(columns={'duration': 'stall'}),
                 'inner'),
        video_lengths,
        'left')
    baseline_qoe['stall'] /= 1000
    baseline_qoe = pd.merge(baseline_qoe,
                            baseline_visible_svar.drop(columns=['segment']).groupby(
                                ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video',
                                 'user', 'tiling', 'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace'],
                                as_index=False).mean(),
                            'inner')
    baseline_qoe = pd.merge(baseline_qoe,
                            baseline_visible_tvar,
                            'inner')
    baseline_qoe['qoe_lin'] = ((baseline_qoe['weighted_q_lin'] * baseline_qoe['length']) / (
            (baseline_qoe['stall'] + baseline_qoe['length']) * H_lin)) * (
                                      1 - (baseline_qoe['spatial_q_lin_variance'] / (2 * H_svar_lin))) * (
                                      1 - (baseline_qoe['temporal_q_lin_variance'] / (2 * H_tvar_lin)))
    baseline_qoe['qoe_log'] = ((baseline_qoe['weighted_q_log'] * baseline_qoe['length']) / (
            (baseline_qoe['stall'] + baseline_qoe['length']) * H_log)) * (
                                      1 - (baseline_qoe['spatial_q_log_variance'] / (2 * H_svar_log))) * (
                                      1 - (baseline_qoe['temporal_q_log_variance'] / (2 * H_tvar_log)))

    baseline_qoe.reset_index(drop=True).to_feather(f'{METRICS_DIR}/{video}/{B_min}/baseline_qoe.feather')

    baseline_qoe_fairness = baseline_qoe.drop(columns=['weighted_q_lin', 'weighted_q_log', 'stall', 'length',
                                                       'spatial_q_lin_variance', 'spatial_q_log_variance',
                                                       'temporal_q_lin_variance', 'temporal_q_log_variance']).groupby(
        ['ABR', 'pred', 'K', 'B_min',
         'B_max', 'video', 'tiling',
         'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace'],
        as_index=False).std()
    baseline_qoe_fairness['fairness_index_lin'] = 1 - (2 * baseline_qoe_fairness['qoe_lin'])
    baseline_qoe_fairness['fairness_index_log'] = 1 - (2 * baseline_qoe_fairness['qoe_log'])
    baseline_qoe_fairness = baseline_qoe_fairness.drop(columns=['qoe_lin', 'qoe_log'])

    baseline_qoe_fairness.reset_index(drop=True).to_feather(
        f'{METRICS_DIR}/{video}/{B_min}/baseline_qoe_fairness.feather')

    del video_lengths
    del baseline_visible_agg
    del baseline_visible_svar
    del baseline_visible_tvar
    del baseline_stall_agg
    del baseline_qoe
    del baseline_qoe_fairness

    bw_util_df = pd.read_feather(f'{CONCATENATED_DIR}/{video}/{B_min}/bw_util.feather')

    baseline_bw = bw_util_df[(bw_util_df['ABR'] == 'baseline') & (bw_util_df['B_max'] == 10)].copy()
    baseline_bw = baseline_bw.drop(
        columns=['cycle', 'cycle_start', 'est_latency', 'est_bw', 'cycle_end', 'est_bw_usage', 'true_bw_usage'])
    baseline_bw['adjusted_cycle_duration'] = baseline_bw['cycle_duration'] - baseline_bw['true_latency']
    baseline_bw['available_bits'] = baseline_bw['true_bw'] * baseline_bw['adjusted_cycle_duration']
    baseline_bw_agg = baseline_bw.drop(columns=['true_latency',
                                                'true_bw',
                                                'cycle_duration',
                                                'adjusted_cycle_duration']).groupby(
        ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video',
         'user', 'tiling', 'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace'],
        as_index=False).sum()
    baseline_bw_agg['bw_usage'] = baseline_bw_agg['bits_dl'] / baseline_bw_agg['available_bits']

    baseline_bw_agg.reset_index(drop=True).to_feather(f'{METRICS_DIR}/{video}/{B_min}/baseline_bw_agg.feather')

    baseline_bw_efficiency_agg = downloaded_df[
        (downloaded_df['ABR'] == 'baseline') & (downloaded_df['B_max'] == 10)].copy()
    baseline_bw_efficiency_agg = baseline_bw_efficiency_agg.drop(
        columns=['segment', 'tile', 'q_lin', 'q_log', 'dl_time',
                 'dl_head', 'first_request_time', 'first_request_head',
                 'last_request_time', 'last_request_head', 'n_requests',
                 'score', 'cycle'])
    baseline_bw_efficiency_agg['hit'] = baseline_bw_efficiency_agg['duration'] > 0
    baseline_bw_efficiency_agg = baseline_bw_efficiency_agg.groupby(
        ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'user',
         'tiling', 'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace', 'hit'],
        as_index=False).sum()
    baseline_bw_efficiency_agg = baseline_bw_efficiency_agg.drop(columns=['duration'])
    baseline_bw_efficiency_agg = \
        baseline_bw_efficiency_agg.set_index(['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'user',
                                              'tiling', 'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace',
                                              'hit'])[
            'size'].unstack().reset_index().rename(columns={False: 'miss', True: 'hit'})
    baseline_bw_efficiency_agg['hit_rate'] = baseline_bw_efficiency_agg['hit'] / (
            baseline_bw_efficiency_agg['hit'] + baseline_bw_efficiency_agg['miss'])

    baseline_bw_efficiency_agg.reset_index(drop=True).to_feather(
        f'{METRICS_DIR}/{video}/{B_min}/baseline_bw_efficiency_agg.feather')

    del downloaded_df
    del bw_util_df
    del baseline_bw
    del baseline_bw_agg
    del baseline_bw_efficiency_agg


def process_all_metrics(n_cpus=max(1, os.cpu_count() - 2)):
    videos_B_mins = []
    for video in sorted(os.listdir(f'{CONCATENATED_DIR}')):
        for B_min in sorted(os.listdir(f'{CONCATENATED_DIR}/{video}')):
            videos_B_mins.append((video, B_min))

    with multiprocessing.Pool(n_cpus) as pool:
        with tqdm(total=len(videos_B_mins)) as pbar:
            for _ in pool.imap_unordered(process_metrics, videos_B_mins):
                pbar.update()


def concatenate_metrics(item):
    metric_df, (video_B_mins) = item
    dataframes = []
    for video, B_min in video_B_mins:
        dataframes.append(pd.read_feather(f'{METRICS_DIR}/{video}/{B_min}/{metric_df}.feather'))
    pd.concat(dataframes).reset_index(drop=True).to_feather(f'{CONCATENATED_METRICS_DIR}/{metric_df}.feather')


def concatenate_all_metrics(n_cpus=max(1, os.cpu_count() - 2)):
    os.makedirs(CONCATENATED_METRICS_DIR, exist_ok=True)
    metrics_dict = {}
    for video in sorted(os.listdir(METRICS_DIR)):
        for B_min in sorted(os.listdir(f'{METRICS_DIR}/{video}')):
            for metric_df in sorted(os.listdir(f'{METRICS_DIR}/{video}/{B_min}')):
                metric_df = metric_df[:-8]
                try:
                    metrics_dict[metric_df].append((video, B_min))
                except KeyError:
                    metrics_dict[metric_df] = [(video, B_min)]

    with multiprocessing.Pool(n_cpus) as pool:
        with tqdm(total=len(metrics_dict)) as pbar:
            for _ in pool.imap_unordered(concatenate_metrics, metrics_dict.items()):
                pbar.update()
