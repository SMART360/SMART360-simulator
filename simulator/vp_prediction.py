# SMART360 Simulator
# Copyright (C) 2023  Quentin Guimard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This file contains code under the following license:
#
# Copyright (c) 2020, UMass-LIDS
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
import numpy as np
import torch

from .dvms import DVMS


class ViewportPredictor:
    def __init__(self):
        pass

    def predict_tiles(self, segment_index):
        raise NotImplementedError

    def update_coord(self, coord):
        pass


class NoPredictor(ViewportPredictor):
    def __init__(self, session_info, headset_model, **kwargs):
        super().__init__()
        n_tiles = int(session_info.get_manifest().n_tiles)
        self.probabilities = np.full((n_tiles, ), 1 / n_tiles)

    def predict_tiles(self, segment_index):
        return self.probabilities


class StaticPredictor(ViewportPredictor):
    def __init__(self, session_info, headset_model, **kwargs):
        super().__init__()
        self.session_info = session_info
        self.headset_config = headset_model.headset_config
        n_tiles = int(session_info.get_manifest().n_tiles)
        self.scores = np.full((n_tiles, ), 1 / n_tiles)

    def update_coord(self, coord):
        self.scores = self.headset_config.expand_fov(self.headset_config.get_tiles(coord))

    def predict_tiles(self, segment_index):
        return self.scores


class DVMSPredictor(ViewportPredictor):
    def __init__(self, session_info, headset_model, **kwargs):
        super().__init__()
        self.session_info = session_info
        self.headset_config = headset_model.headset_config
        manifest = session_info.get_manifest()
        self.tiles_x = int(manifest.tiles_x)
        self.tiles_y = int(manifest.tiles_y)
        self.n_tiles = int(manifest.n_tiles)
        self.r = kwargs['r']
        self.K = kwargs['K']
        self.model = DVMS(3, 25, n_samples_train=self.K).float().to('cpu')
        self.model.load_state_dict(torch.load(f'dvms_weights/dvms_k{self.K}.pth', map_location='cpu'))
        self.past_pos = []
        self.predicted_future_pos = None
        self.pred_tiles = None
        self.past_pred = []
        self.likelihoods = np.full(self.K, 1 / self.K)
        self.last_coord_time = None
        self.tmp_scores = np.full((self.n_tiles, ), 1 / self.n_tiles)

    def compute_likelihood(self):
        past_pos = np.expand_dims(np.array(self.past_pos)[-len(self.past_pred):], 0).repeat(self.K, 0)
        past_pred = np.array(self.past_pred)[0, :, :len(self.past_pred)]
        past_err = (2 * np.arcsin((np.linalg.norm(past_pos - past_pred, axis=-1) / 2).clip(0.0, 1.0))).mean(-1)
        standardized_past_err = (past_err - past_err.mean()) / past_err.std()
        nexp_past_err = np.exp(-standardized_past_err)
        return nexp_past_err / nexp_past_err.sum()

    def update_coord(self, coord):
        self.past_pos.append(coord)
        self.last_coord_time = float(self.session_info.get_buffer().get_play_head())
        if len(self.past_pos) >= 6:
            if len(self.past_pos) > 6 and self.K > 1:
                self.past_pred.append(self.predicted_future_pos[:self.r])
                if len(self.past_pred) > self.r:
                    del self.past_pred[0]
                self.likelihoods = self.compute_likelihood()
            self.predicted_future_pos = self.model.sample([torch.Tensor([self.past_pos[-6:-1]]),
                                                           torch.Tensor([self.past_pos[-1:]])]).tolist()
            self.pred_tiles = [[self.headset_config.get_tiles(pos) for pos in traj] for traj in self.predicted_future_pos]
        else:
            self.tmp_scores = self.headset_config.expand_fov(self.headset_config.get_tiles(coord))

    def predict_tiles(self, segment_index):
        if not self.past_pos or len(self.past_pos) < 6:
            return self.tmp_scores
        segment_time = float(segment_index * self.session_info.get_buffer().segment_duration)
        offset = min(max(int(np.round((segment_time - self.last_coord_time) / 200)), 0), 20)
        tile_fov = np.zeros(self.n_tiles)
        for k in range(self.K):
            likelihood = self.likelihoods[k]
            for i in range(5):
                tile_fov += (np.array(self.pred_tiles[k][offset + i]) * likelihood * 0.2)
        return self.headset_config.expand_fov(tile_fov / tile_fov.max())
