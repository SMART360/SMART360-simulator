# SMART360 Simulator
# Copyright (C) 2023  Quentin Guimard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This file contains code under the following license:
#
# Copyright (c) 2020, UMass-LIDS
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
class NetworkModel:
    def __init__(self, network_trace):
        self.network_total_time = 0
        self.trace = network_trace
        self.index = -1
        self.curr_bw = None
        self.curr_latency = None
        self.time_to_next = 0
        self._next_network_period()

    def _next_network_period(self):
        self.index += 1
        if self.index == len(self.trace):
            self.index = 0
        self.time_to_next = self.trace[self.index].time
        self.curr_bw = self.trace[self.index].bandwidth
        self.curr_latency = self.trace[self.index].latency

    def _true_network_metrics(self, time):
        bits = 0
        time_left = time
        i = self.index
        time_to_next = min(time_left, self.trace[i].time - self.time_to_next)
        latency = self.curr_latency
        while time_left > 0:
            bits += self.trace[i].bandwidth * time_to_next
            time_left -= time_to_next
            if self.trace[i].bandwidth > 0:
                latency = self.trace[i].latency
            i -= 1
            time_to_next = min(time_left, self.trace[i].time)
        return latency, bits / time

    def delay_network(self, time):  # time spent not downloading
        while time > 0:
            elapsed = min(self.time_to_next, time)
            self.time_to_next -= elapsed
            self.network_total_time += elapsed
            time -= elapsed
            if self.time_to_next == 0:
                self._next_network_period()

    def _elapse_latency(self):
        time_elapsed = 0
        remaining_latency = self.curr_latency  # Assuming duration_ms cannot be shorter than latency_ms, hence no need for a loop
        if remaining_latency > self.time_to_next:
            time_elapsed += self.time_to_next
            remaining_latency -= self.time_to_next
            self.network_total_time += self.time_to_next
            former_latency = self.curr_latency
            self._next_network_period()
            new_latency = self.curr_latency
            remaining_latency += max(0, new_latency - former_latency)
        elif remaining_latency == self.time_to_next:
            time_elapsed += self.time_to_next
            remaining_latency = 0
            self.network_total_time += self.time_to_next
            self._next_network_period()
        time_elapsed += remaining_latency
        self.network_total_time += remaining_latency
        self.time_to_next -= remaining_latency
        return time_elapsed

    def download_group_of_tiles(self, dl_schedule_and_sizes):  # Used in startup and stall, no time constraint -> return time elapsed
        time_elapsed_latency = self._elapse_latency()
        time_elapsed_dl = 0
        while dl_schedule_and_sizes:
            if self.curr_bw == 0:
                time_elapsed_dl += self.time_to_next
                self.network_total_time += self.time_to_next
                self._next_network_period()
                continue
            schedule_element = dl_schedule_and_sizes.pop()
            size_left = schedule_element['size_left']
            provisional_time_ms = size_left / self.curr_bw
            if provisional_time_ms > self.time_to_next:
                allowed_download = self.time_to_next * self.curr_bw
                schedule_element['size_left'] = size_left - allowed_download
                dl_schedule_and_sizes.append(schedule_element)
                time_elapsed_dl += self.time_to_next
                self.network_total_time += self.time_to_next
                self._next_network_period()
            elif provisional_time_ms == self.time_to_next:
                time_elapsed_dl += self.time_to_next
                self.network_total_time += self.time_to_next
                self._next_network_period()
            else:
                time_elapsed_dl += provisional_time_ms
                self.network_total_time = self.network_total_time + provisional_time_ms
                self.time_to_next = self.time_to_next - provisional_time_ms
        return time_elapsed_latency, time_elapsed_dl

    def request_group_of_tiles(self):
        return self._elapse_latency()

    def download_group_of_tiles_time_constrained(self, dl_schedule_and_sizes, max_time):
        time_left = max_time
        time_elapsed_dl = 0
        downloaded_bits = 0
        downloaded_elements = []
        while dl_schedule_and_sizes and time_left > 0:
            time_to_next_it = min(time_left, self.time_to_next)
            if self.curr_bw == 0:
                time_left -= time_to_next_it
                time_elapsed_dl += time_to_next_it
                self.time_to_next -= time_to_next_it
                self.network_total_time += time_to_next_it
                if self.time_to_next == 0:
                    self._next_network_period()
                continue
            schedule_element = dl_schedule_and_sizes.pop()
            size_left = schedule_element['size_left']
            provisional_time_ms = size_left / self.curr_bw
            if provisional_time_ms > time_to_next_it:
                allowed_download = time_to_next_it * self.curr_bw
                schedule_element['size_left'] = size_left - allowed_download
                dl_schedule_and_sizes.append(schedule_element)
                time_left -= time_to_next_it
                time_elapsed_dl += time_to_next_it
                downloaded_bits += allowed_download
                self.time_to_next -= time_to_next_it
                self.network_total_time += time_to_next_it
            else:
                time_left -= provisional_time_ms
                time_elapsed_dl += provisional_time_ms
                downloaded_bits += size_left
                self.network_total_time += provisional_time_ms
                self.time_to_next -= provisional_time_ms
                schedule_element['size_left'] = 0
                downloaded_elements.append(schedule_element)
            if self.time_to_next == 0:
                self._next_network_period()
        return time_elapsed_dl, downloaded_bits, downloaded_elements
