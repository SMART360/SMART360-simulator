# SMART360 Simulator
# Copyright (C) 2023  Quentin Guimard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This file contains code under the following license:
#
# Copyright (c) 2020, UMass-LIDS
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from decimal import Decimal
from fractions import Fraction
import os

import simplejson as json


def decimal_from_fraction(frac):
    return frac.numerator / Decimal(frac.denominator)


class LogFile:
    def __init__(self, session_info):
        self.session_info = session_info
        self.records = []

    def dump(self, path):
        os.makedirs(os.path.dirname(path), exist_ok=True)
        with open(path, 'w') as f:
            json.dump(self.records, f, use_decimal=True)

    def log_dict(self, d):
        for k, v in d.items():
            if type(v) == Fraction:
                d[k] = decimal_from_fraction(v)
            elif type(v) in [list, tuple]:
                new_value = []
                for e in v:
                    if type(e) == Fraction:
                        new_value.append(decimal_from_fraction(e))
                    else:
                        new_value.append(e)
                d[k] = new_value
        self.records.append({**d, 'wall_time': decimal_from_fraction(self.session_info.get_wall_time()),
                             'play_head': decimal_from_fraction(self.session_info.get_buffer().get_play_head())})

    def log_any(self, category, value):
        self.log_dict({'category': category, 'value': value})

    def log_new_cycle(self, index):
        pass
        # self.log_any('cycle', index)

    def log_coord(self, coord):
        pass
        # self.log_any('coord', coord)

    def log_visible_tiles(self, tiles):
        self.log_any('visible_tiles', tiles.tolist())

    def log_buffer_head(self, segment_tiles):
        pass
        # self.log_any('buffer_head', segment_tiles.tolist())

    def log_visible_quality(self, visible_quality):
        pass
        # self.log_any('visible_quality', visible_quality.tolist())

    def log_network_estimation(self, latency, bandwidth, cycle):
        self.log_any('network_estimation', (latency, bandwidth, cycle))

    def log_network_actual(self, latency, bandwidth, cycle):
        self.log_any('network_actual', (latency, bandwidth, cycle))

    def log_pred(self, segment, tile_prob):
        self.log_any('tile_prediction', (segment, tile_prob.tolist()))

    def log_dl_schedule(self, dl_schedule_list):
        compact_dl_schedule = [[int(v) for v in e.values()] for e in dl_schedule_list]
        self.log_any('dl_schedule', compact_dl_schedule)

    def log_startup(self, startup_time, downloaded_bits):
        self.log_any('startup', (startup_time, downloaded_bits))

    def log_stall(self, stall_time, downloaded_bits):
        self.log_any('stall', (stall_time, downloaded_bits))

    def log_dl_schedule_left(self, dl_schedule_list):
        pass
        # compact_dl_schedule = [[int(v) for v in e.values()] for e in dl_schedule_list]
        # self.log_any('dl_schedule_left', compact_dl_schedule)

    def log_dl_success(self, dl_success_list):
        compact_dl_list = []
        for e in dl_success_list:
            s, t, q, *_ = map(int, e.values())
            compact_dl_list.append([s, t, q])
        self.log_any('dl_success', compact_dl_list)

    def log_buffer(self, buffer):
        pass
        # self.log_any('buffer', (buffer.get_played_segments(), buffer.get_buffer_content().tolist()))

    def log_now_playing(self, time):
        self.log_any('playing_video', time)

    def log_time_latency(self, time):
        pass
        # self.log_any('request_latency', time)

    def log_time_downloading(self, time):
        pass
        # self.log_any('download_time', time)

    def log_bits_downloaded(self, bits):
        pass
        # self.log_any('bits_downloaded', bits)
