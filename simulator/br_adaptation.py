# SMART360 Simulator
# Copyright (C) 2023  Quentin Guimard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This file contains code under the following license:
#
# Copyright (c) 2020, UMass-LIDS
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
import numpy as np


class TiledABR:
    def __init__(self):
        pass

    def startup_dl_schedule(self):
        raise NotImplementedError

    def stall_dl_schedule(self, blank_tiles):
        raise NotImplementedError

    def decide_dl_schedule(self, est_latency, est_tput, delta_dl):
        raise NotImplementedError


class MaxStallABR(TiledABR):
    def __init__(self, config, session_info):
        self.session_info = session_info
        super().__init__()

    def startup_dl_schedule(self):
        n_tiles = int(self.session_info.get_manifest().n_tiles)
        return [{'segment': 0, 'tile': i, 'quality': 0} for i in range(n_tiles)]

    def stall_dl_schedule(self, blank_tiles):
        curr_seg = self.session_info.get_buffer().get_played_segments()
        return [{'segment': int(curr_seg), 'tile': tile, 'quality': 0} for tile in blank_tiles]

    def decide_dl_schedule(self, est_latency, est_tput, delta_dl):
        return []


class TrivialABR(TiledABR):
    def __init__(self, config, session_info):
        self.session_info = session_info
        self.max_seg = len(session_info.get_manifest().segments) - 1

        super().__init__()

    def startup_dl_schedule(self):
        n_tiles = int(self.session_info.get_manifest().n_tiles)
        return [{'segment': 0, 'tile': i, 'quality': 0} for i in range(n_tiles)]

    def stall_dl_schedule(self, blank_tiles):
        curr_seg = self.session_info.get_buffer().get_played_segments()
        return [{'segment': int(curr_seg), 'tile': tile, 'quality': 0} for tile in blank_tiles]

    def decide_dl_schedule(self, est_latency, est_tput, delta_dl):
        buffer = self.session_info.get_buffer()
        curr_seg = buffer.get_played_segments()
        schedule = []
        for buf_seg, tile in np.argwhere(buffer.get_buffer_content() == -1):
            seg = int(buf_seg + curr_seg)
            if seg <= self.max_seg:
                schedule.append({'segment': seg, 'tile': tile, 'quality': 0})
        return schedule


class BaselineABR(TiledABR):
    def __init__(self, config, session_info):
        self.session_info = session_info
        self.B_min = int(config['B_min'])
        self.max_seg = len(session_info.get_manifest().segments) - 1
        self.max_quality = len(session_info.get_manifest().bitrates) - 1
        self.segment_sizes = np.array(session_info.get_manifest().segments)
        self.viewport_predictor = session_info.get_viewport_predictor()
        self.score_thresholds = (.2, .4, .6, .8, 1)

        super().__init__()

    def startup_dl_schedule(self):
        n_tiles = int(self.session_info.get_manifest().n_tiles)
        return [{'segment': 0, 'tile': i, 'quality': 0} for i in range(n_tiles)]

    def stall_dl_schedule(self, blank_tiles):
        curr_seg = self.session_info.get_buffer().get_played_segments()
        return [{'segment': int(curr_seg), 'tile': tile, 'quality': 0} for tile in blank_tiles]

    def _allocate_budget_heuristic(self, budget, buffer, curr_seg, pred_scores):
        segments, tiles = np.argwhere(buffer == -1).T
        schedule = np.full_like(buffer, -1, dtype=np.int8)

        bool_segments_B_min = segments < self.B_min
        segments_B_min = segments[bool_segments_B_min]
        tiles_B_min = tiles[bool_segments_B_min]
        B_min_cost = self.segment_sizes[curr_seg + segments_B_min, tiles_B_min, 0].sum()

        if budget <= B_min_cost:
            schedule[segments_B_min, tiles_B_min] = 0
        else:
            schedule[segments, tiles] = self.max_quality
            cost = self.segment_sizes[curr_seg + segments, tiles, self.max_quality].sum()
            if budget < cost:
                i = -1
                while budget < cost:
                    i += 1
                    prev_schedule = schedule.copy()
                    schedule[pred_scores <= self.score_thresholds[min(i, self.max_quality)]] -= 1
                    schedule = schedule.clip(-1)
                    schedule[segments_B_min, tiles_B_min] = schedule[segments_B_min, tiles_B_min].clip(0)
                    segments, tiles = np.argwhere(schedule != -1).T
                    qualities = schedule[segments, tiles]
                    cost = self.segment_sizes[curr_seg + segments, tiles, qualities].sum()
                if budget > cost:
                    last_downgraded_s, last_downgraded_t = np.argwhere(prev_schedule != schedule).T
                    threshold_distances = (np.digitize(pred_scores[last_downgraded_s, last_downgraded_t], (0,) + self.score_thresholds, right=True) / 5) - pred_scores[last_downgraded_s, last_downgraded_t]
                    sorted_last_downgraded = np.argsort(threshold_distances, kind='stable')
                    for idx in sorted_last_downgraded:
                        s, t = last_downgraded_s[idx], last_downgraded_t[idx]
                        schedule[s, t] += 1
                        cost += (self.segment_sizes[curr_seg + s, t, schedule[s, t]] - self.segment_sizes[curr_seg + s, t, schedule[s, t] - 1])
                        if cost >= budget:
                            break
        return schedule

    def _order_ABR_schedule(self, dense_schedule, curr_seg, pred_scores):
        ordered_schedule = []
        for segment, segment_qualities in enumerate(dense_schedule):
            tiles = np.argwhere(segment_qualities != -1).flatten()
            tile_scores = pred_scores[segment, tiles]
            sorted_tiles = tiles[np.argsort(tile_scores, kind='stable')[::-1]]
            for tile in sorted_tiles:
                quality = dense_schedule[segment, tile]
                ordered_schedule.append({'segment': segment + curr_seg, 'tile': tile, 'quality': quality})
        return ordered_schedule

    def decide_dl_schedule(self, est_latency, est_tput, delta_dl):
        available_bits = est_tput * (delta_dl - est_latency)
        buffer = self.session_info.get_buffer()
        curr_seg = buffer.get_played_segments()
        buffer = buffer.get_buffer_content()[:self.max_seg - curr_seg + 1]
        segments, _ = np.argwhere(buffer == -1).T
        if len(segments) == 0:
            return []
        pred_segments = np.unique(segments)
        pred_scores = []
        for segment in range(len(buffer)):
            if segment in pred_segments:
                if segment <= 5:
                    pred_scores.append(self.viewport_predictor.predict_tiles(segment + curr_seg))
                else:
                    if set(range(5, segment)) & set(pred_segments):
                        pred_scores.append(pred_scores[-1])
                    else:
                        pred_scores.append(self.viewport_predictor.predict_tiles(segments[-1] + curr_seg))
                self.session_info.log_file.log_pred(segment + curr_seg, pred_scores[-1])
            else:
                pred_scores.append(np.zeros(buffer.shape[1]))
        pred_scores = np.stack(pred_scores)

        dense_schedule = self._allocate_budget_heuristic(available_bits, buffer, curr_seg, pred_scores)
        return self._order_ABR_schedule(dense_schedule, curr_seg, pred_scores)
